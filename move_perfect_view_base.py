#! /usr/bin/env python3

"""
memory_conceptual
memory_episodic
memory_procedural
memory_perceptive
memory_immediate

emotional_state

sensors = [eyes, skin, power_unit]
effectors = [legs]

environment

context_spatiotemporal
context_procedural
context_conceptual

objective
decision_making
needs

messages

Approach 1:
agent move around obstacles, view range grid perfect, obstacles grid perfect
"""

import sys
from itertools import product as iproduct 

import yaml
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import pygame
#from pygame.locals import *

def yml(data):
    return yaml.safe_load(data)

real = {}
real["map"] = np.array(list(zip(*[
[0, 1, 1, 0, 1, 1, 0, 1],
[0, 1, 0, 0, 0, 1, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[1, 0, 0, 0, 0, 1, 0, 0],
[1, 0, 1, 0, 1, 1, 0, 1]
])))
real["pos"] = [4, 2]
real["direc"] = 1 # UP, RIGHT, DOWN, LEFT
brain = {}

# Helpers

vadd = np.add
vmul = np.multiply

def direc2vec(direc):
    if direc == 0:   newx, newy = 0, -1
    elif direc == 1: newx, newy = 1, 0
    elif direc == 2: newx, newy = 0, 1
    elif direc == 3: newx, newy = -1, 0
    return [newx, newy]

def bounding_rect(pos_list):
    xs = [x for x, _ in pos_list]
    ys = [y for _, y in pos_list]
    return (min(xs), min(ys), max(xs), max(ys))

# Logic

def process_inputs(brain, inputs):
    for typ, val in inputs:
        # view_real format: grid
        if typ == "view_real":
            if val not in brain["environment"]["map"]:
                brain["environment"]["map"].append(val)
            brain["memory_perceptive"]["visual"].append((typ, val))

# Simulation

def gen_view_real():
    mental_map = brain["environment"]["map"]
    real_map = real["map"]
    real_pos = real["pos"]
    real_direc = real["direc"]
    
    fpos = vadd(real_pos, vmul(3, direc2vec(real_direc)))
    lpos = vadd(real_pos, direc2vec((real_direc - 1) % 4))
    rpos = vadd(real_pos, direc2vec((real_direc + 1) % 4))
    minx, miny, maxx, maxy = bounding_rect([real_pos, fpos, lpos, rpos])
    idx = list(iproduct(range(max(minx, 0), min(maxx+1, len(real_map))), 
                        range(max(miny, 0), min(maxy+1, len(real_map[0])))))
    inputs = [("view_real", ((x, y), real_map[x, y])) for x, y in idx]
    #print(inputs)
    return inputs

def update():
    # Reset perceptive memory
    brain["memory_perceptive"]["visual"] = []
    
    mental_map = brain["environment"]["map"]
    real_map = real["map"]
    real_pos = real["pos"]
    real_direc = real["direc"]
    mapxlen = len(real_map)
    mapylen = len(real_map[0])
    
    # Compute robot sight range
    inputs = gen_view_real()
    
    process_inputs(brain, inputs)
    #print(mental_map)

TILE_SIZE = 64
GREY4 = (100, 100, 100)
BROWN5 = (200, 150, 50)
GREEN2 = (50, 250, 100)
GREEN8 = (0, 150, 50)
BLUE3 = (0, 150, 200)
BLUE7 = (50, 50, 200)
YELLOW7 = (150, 150, 0)
RED3 = (250, 50, 50)
RED7 = (200, 0, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# GUI

def gui_sdl_input(event):
    pos = x, y = real["pos"]
    direc = real["direc"]
    mapp = real["map"]
    xmax = len(real["map"]) - 1
    ymax = len(real["map"][0]) - 1
    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_LEFT:
            real["direc"] = (direc - 1) % 4
        if event.key == pygame.K_RIGHT:
            real["direc"] = (direc + 1) % 4
        if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
            if event.key == pygame.K_DOWN: direc = (direc + 2) % 4 # temp
            newx, newy = vadd(pos, direc2vec(direc))
            if 0 <= newx <= xmax and 0 <= newy <= ymax and mapp[newx, newy] == 0:
                real["pos"][:] = newx, newy

def gui_sdl_draw(display_surf):
    mapp = brain["environment"]["map"]
    cur_view = [pos for (_, (pos, _)) in brain["memory_perceptive"]["visual"]]
    COLOR_MAP = {'P': BLUE7, 0: WHITE, 1: RED7}
    #print("cur_view:", cur_view)
    #print("mapp:", mapp)
    
    display_surf.fill(BLACK)
    # Draw map
    for pos, val in mapp:
        if pos in cur_view:
            pygame.draw.rect(display_surf, COLOR_MAP[val], 
                vmul([*pos, 1, 1], TILE_SIZE))
        else:
            pygame.draw.rect(display_surf, vmul(COLOR_MAP[val], .6), 
                vmul([*pos, 1, 1], TILE_SIZE))
    # Draw robot
    pygame.draw.rect(display_surf, COLOR_MAP['P'], 
        vmul([*real["pos"], 1, 1], TILE_SIZE))
    line_start = vadd(real["pos"], .5)
    line_end = vadd(line_start, vmul(.5, direc2vec(real["direc"])))
    pygame.draw.line(display_surf, WHITE, 
        vmul(line_start, TILE_SIZE), vmul(line_end, TILE_SIZE), 4)

    pygame.display.update()

def gui_sdl():
    pygame.init()
    display_surf = pygame.display.set_mode(vmul(real["map"].shape, TILE_SIZE))
    clock = pygame.time.Clock()

    update()
    gui_sdl_draw(display_surf)
    dt = clock.tick(1)

    while True:
        for event in pygame.event.get():
            gui_sdl_input(event)
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        update()
        gui_sdl_draw(display_surf)
        
        # Tick and update caption
        dt = clock.tick(2)
        pygame.display.set_caption("Cognition Lab 2 | FPS {:.1f}".format(clock.get_fps()))

# Entry point

def main():
    for k, v in {
    "memory_conceptual": {},
    "memory_episodic": {},
    "memory_procedural": {},
    "memory_perceptive": {"visual": {}},
    "memory_immediate": {},
    "context_spatiotemporal": {},
    "context_procedural": {},
    "context_conceptual": {},
    "environment": {"map": []},
    "sensors": yml("[eyes, skin, power_unit]"),
    "effectors": yml("[legs]"),
    }.items():
        brain[k] = v
    
    gui_sdl()

#g = nx.Graph({})
#nx.draw_networkx(g, pos={})
#plt.axis('off')
#plt.show()

if __name__ == "__main__":
    main()
