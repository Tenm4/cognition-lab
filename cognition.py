#! /usr/bin/env python3

"""
Model:

memory_conceptual
memory_episodic
memory_procedural
memory_perceptive
memory_immediate

emotional_state

sensors = [eyes, skin, power_unit]
effectors = [legs]

environment

context_spatiotemporal
context_procedural
context_conceptual

objective
decision_making
needs

messages

Approach 1:

agent move around obstacles, view range grid perfect, obstacles grid perfect
- env perception + work memo
- (re)cognization + memo/reinfcmt (episodic)
- spatialization / env mapping / paths construction (pos + angles) + memo
- decision based on spatial situation

How to construct spatial map?
- cognize surroundings (what is open/blocked?)
- find potential path
- move in non-obstacle direction (angle)
- construct a path as a trail

Bloody problems:
- how to cognize an obstacle or a path ? get key elements of surroundings
- how to locate oneself mentally ? try to recognize which is fuzzy, based on episodic memory (axis: recent places, cannot locate absolutely without arbiter
- how to move better than alea + contact senses (plz help Im dumb!)? 

Positionning: 3rd person absolute, 3rd person relative, 1st person (neither absolute nor relative) but envtal

TODO :
- use polar vectors (pvec) ≠ cartesian vectors (cvec)
- more effective algo to raycast a space than to get union of N raycasts paths
"""

import sys, math
from pprint import pprint
from itertools import product as iproduct, cycle as icycle, islice, chain as ichain

import yaml
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import pygame, pygame.freetype

from vec2d import Vec2d

def yml(data):
    return yaml.safe_load(data)

real = {}
real["map"] = np.array(list(zip(*[
[0, 0, 0, 0, 1, 1, 1, 1],
[0, 1, 1, 0, 1, 1, 0, 0],
[0, 1, 0, 0, 0, 1, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 1, 0, 1],
[1, 0, 0, 0, 1, 1, 0, 0],
[1, 0, 1, 0, 1, 0, 0, 1]
])))
real["pos"] = Vec2d(4, 3)
real["direcq"] = 1 # UP, RIGHT, DOWN, LEFT = clocktime quadrants
real["sight"] = 3
real["motion"] = {"now": "none"}
mens = {}
gui = {}

# Helpers

vadd = np.add
vmul = np.multiply

def direcq2vec(direcq):
    if direcq == 0:   newx, newy = 0, -1
    elif direcq == 1: newx, newy = 1, 0
    elif direcq == 2: newx, newy = 0, 1
    elif direcq == 3: newx, newy = -1, 0
    return Vec2d(newx, newy)

def direcq2angle(direcq):
    # clocktime quadrants to radians
    return ((1 - direcq) % 4) * math.pi / 2

def bounding_rect(pos_list):
    xs = [x for x, _ in pos_list]
    ys = [y for _, y in pos_list]
    return (min(xs), min(ys), max(xs), max(ys))

def side2edge(mappos, side):
    corners = [Vec2d(0, 0), Vec2d(1, 0), Vec2d(1, 1), Vec2d(0, 1)]
    return (mappos + corners[side], mappos + corners[(side + 1) % 4])

def corner2vertex(mappos, corner):
    corners = [Vec2d(0, 0), Vec2d(1, 0), Vec2d(1, 1), Vec2d(0, 1)]
    return mappos + corners[corner]

def no_collide_map(pos, mode=0):
    real_map = real["map"]
    x, y = pos
    w, h = len(real_map), len(real_map[0])
    if mode == 0: return 0 <= x < w and 0 <= y < h and real_map[x, y] == 0
    if mode == 1: return 0 <= x < w and 0 <= y < h
    if mode == 2: return real_map[x, y] == 0
    raise Exception("Logic error: mode = %s" % mode)

# Perception

def raycast_dda(pos, direcv, mapp, do_path=False):
    # TODO: relative collision pos?
    ray_dir = Vec2d(direcv) + .00001
    fpos = Vec2d(pos)
    ipos = fpos.trunc()
    step = ray_dir.signum()
    # length multiplier of ray from one x or y-side to next x or y-side
    delta_dist = abs(1 / ray_dir)
    # length multiplier of ray from origin to next x or y-side
    side_dist = Vec2d(
        ((pos.x - ipos.x) if ray_dir.x < 0 else (1 + ipos.x - pos.x)), 
        ((pos.y - ipos.y) if ray_dir.y < 0 else (1 + ipos.y - pos.y))
        ) * delta_dist
    path = []
    # print(ray_dir, ipos, step, delta_dist, side_dist, side_dist * ray_dir)
    while no_collide_map(fpos.intco, mode=0):
        if side_dist.x < side_dist.y:
            fpos.x += step.x
            side = 3 if step.x > 0 else 1
            path.append((Vec2d(fpos), ray_dir * side_dist.x, side))
            side_dist.x += delta_dist.x
        else:
            fpos.y += step.y
            side = 0 if step.y > 0 else 2
            path.append((Vec2d(fpos), ray_dir * side_dist.y, side))
            side_dist.y += delta_dist.y
        # print(fpos, side_dist, side_dist * ray_dir, (pos + side_dist * ray_dir))
    # print(len(path))
    return path if do_path else path[-1]

# possible algos for get_sight:
# 1. as we are on a grid, start with upper-left point of bounding rect, linescan until bottom right, applying disk sector test on cells
# 2. get path of bdirec, then get all cells that are clockwise to found cells by inc/dec x/y clockwise, until edirec
# 3. get path of bdirec, then raycast all cells that are clockwise by rotating clockwise current direc a step (cell size), until edirec

def get_sight2():
    mapp = real["map"]
    map_pos = real["pos"]
    mdirecv = Vec2d(direcq2vec(real["direcq"]))
    sight_range = real["sight"]
    semi_angle = math.pi / 4 + .01
    origin = map_pos + .5
    # main/begin/end direction
    bdirecv = mdirecv.rotated(-semi_angle)
    edirecv = mdirecv.rotated(semi_angle)
    bnormal = bdirecv.perpl()
    enormal = edirecv.perpr()
    gui["mdirecv"] = mdirecv
    gui["bdirecv"] = bdirecv
    gui["edirecv"] = edirecv
    gui["semi_angle"] = semi_angle
    gui["radius"] = sight_range

    corners_vec = [Vec2d(-.5, -.5), Vec2d(.5, -.5), Vec2d(.5, .5), Vec2d(-.5, .5)]
    fpos = Vec2d(-sight_range, -sight_range)
    poss = []
    #print(map_pos, origin, sight_range, mdirecv, bdirecv, edirecv)
    while fpos.y <= sight_range:
        #print("%s %s %.2f %.2f %.2f" % (fpos.intco, fpos.floatco, fpos.magnitude, fpos.perprness(bdirecv), fpos.perprness(edirecv)))
        if fpos.x > sight_range:
            fpos.y += 1
            fpos.x = -sight_range
        else:
            corners = [fpos + cv for cv in corners_vec]
            if any(c.magnitude <= sight_range for c in corners): # radius test
                # betweenness clockwise test
                if any(c.perprness(bdirecv) >= 0 and c.perprness(edirecv) <= 0 for c in corners):
                    if no_collide_map(map_pos + fpos.trunc(), mode=0):
                        poss.append(map_pos + fpos.trunc())
                    #print("OK")
            fpos.x += 1
    #print(' '.join(str(pos.intco) for pos in poss))
    return poss

def get_sight():
    mapp = real["map"]
    map_pos = real["pos"]
    mdirecv = Vec2d(direcq2vec(real["direcq"]))
    sight_range = real["sight"]
    semi_angle = math.pi / 4 + .01
    origin = map_pos + .5
    # main/begin/end direction
    bdirecv = mdirecv.rotated(-semi_angle)
    edirecv = mdirecv.rotated(semi_angle)
    bnormal = bdirecv.perpl()
    enormal = edirecv.perpr()
    gui["mdirecv"] = mdirecv
    gui["bdirecv"] = bdirecv
    gui["edirecv"] = edirecv
    gui["semi_angle"] = semi_angle
    gui["radius"] = sight_range
    
    poss = set()
    nb_rays = 17
    for i in range(nb_rays):
        angle = semi_angle * 2 / (nb_rays - 1) * i
        path = raycast_dda(origin, bdirecv.rotated(angle), mapp, True)
        gui["agent_raycasts"] += path[-1:]
        poss.update(fpos.trunc() for fpos, _, _ in path if no_collide_map(fpos.intco, mode=1))
    
    # print("agent_raycasts:", *(' '.join(map(str, raycast)) for raycast in gui["agent_raycasts"]), sep='\n')
    # print("poss:", poss)
    return poss

def process_inputs(mens, inputs):
    for typ, val in inputs:
        # view_real format: grid
        if typ == "view_real":
            mens["memory_perceptive"]["visual"] = (typ, val)
            mens["environment"]["rawmap"].update(val)
            # transform viewed info to env mapping
            

# Spatial Cognization

def visibility(pos, do_path=False, ray_nb=8):
    """How shiny am I? Raycast 'em all."""
    mapp = real["map"]
    
    delta_angle = -math.pi * 2 / ray_nb
    direcv = Vec2d.x_unit()
    raycasts = []
    for _ in range(ray_nb):
        raycast = raycast_dda(pos, direcv, mapp, do_path=do_path)
        # if do_path: gui["pos_raycasts"].extend((pos, r) for r in raycast)
        # else: gui["pos_raycasts"].append((pos, raycast))
        raycasts.append(raycast)
        direcv.rotate(delta_angle)
    return raycasts
    
def visibility_coef(pos, ray_nb=8):
    raycasts = visibility(pos, do_path=False, ray_nb=ray_nb)
    obstacles_pos = [pos for pos, _, _ in raycasts]
    obstacles_distsqrd = [ray_vec.get_magnitude_sqrd() for _, ray_vec, _ in raycasts]
    return len(obstacles_pos) / np.mean(obstacles_distsqrd)
    
def find_local_visibility_center(pos, threshold=.5):
    """Global vs local measure. Need min threshold. How many rays? Precision vs speed. Pandirectional dichotomy? Rays of rays? Until visibility polygon max area? Count distance negatively to lean towards rounder areas?"""
    mapp = real["map"]
    ray_nb = 6
    print("Agent Pos:", pos)
    
    # open_pos, closed_pos = set([pos]), set() # Dijkstra
    # while True:
        # dpos = open_pos.pop()
        # closed_pos.add(dpos)
    # 2-level raycasting
    # visibility_center must be visible from any pos in room
    # important that pos be cell center, otherwise too many rooms
    # so, raycast to find center candidates, then assess their viz betweenness 
    vizcoefs = {}
    center_candidates = visibility(pos, do_path=True, ray_nb=1) # min 6 best 20
    gui["pos_raycasts"].extend((pos, BLUE3, rp[-1]) for rp in center_candidates)
    print("center_candidates:"); pprint(center_candidates)
    center_candidates = set(ichain.from_iterable(c[:-1] for c in center_candidates))
    center_candidates.add((pos, Vec2d.zero(), -1))
    print("center_candidates:"); pprint(center_candidates)
    for i, (coll_inner_pos, ray_vec, side) in enumerate(center_candidates):
        cand_inner_pos = coll_inner_pos
        cand_peri_pos = pos + ray_vec - ray_vec.signum()/1e3  # before the wall
        print(cand_inner_pos, cand_peri_pos)
        
        # TODO visibility at center, not collision
        cand_raycasts = visibility(cand_peri_pos, do_path=True, ray_nb=ray_nb)
        print("obstacles_pos:", len(cand_raycasts), [[pos for pos, _, _ in path] for path in cand_raycasts])
        cand_raycasts = set(ichain.from_iterable(r[:-1] for r in cand_raycasts))
        # cand_raycasts = list(ichain.from_iterable(cand_raycasts))
        color = tuple(map(int, vmul(GREEN2, i / len(center_candidates))))
        print("color", color)
        gui["pos_raycasts"].extend((cand_peri_pos, color, r) for r in cand_raycasts)
        
        obstacles_pos = [pos for pos, _, _ in cand_raycasts]
        obstacles_rayvecs = [ray_vec for _, ray_vec, _ in cand_raycasts]
        obstacles_distsqrd = [ray_vec.get_magnitude() for _, ray_vec, _ in cand_raycasts]
        print("obstacles_pos:", len(obstacles_pos), obstacles_pos)
        print("obstacles_rayvecs:", obstacles_rayvecs)
        print("obstacles_distsqrd:", ' '.join("%.2f" % d for d in obstacles_distsqrd))
        
        vizcoef = np.mean(obstacles_distsqrd) if obstacles_pos else 0
        print(cand_inner_pos, vizcoef); print()
        vizcoefs[cand_inner_pos] = vizcoef
        gui["vizcoef"][cand_inner_pos.intco] = vizcoef

def build_spatial_graph():
    """How to build spatial map?
    - cognize surroundings (what is open/blocked?)
    - find potential path
    - move in non-obstacle direction (angle)
    - construct paths from the trail
    
    Path is adjacent segments as list of vectors (exact) or recent episodic memory (fuzzy) 
    Visibility graph whose nodes are spots identified by cvec/pvec (exact) or spot-cog (fuzzy)
    
    Exact version: accum paths from trail, add intersections when occur.
    
    Visibility center = max local visibility coeff determines spot. Heuristic?
    How to compute? Incremental?

    Visibility graph construction pseudocode:
    
    trail = ...
    paths = ...
    last_path = paths[-1]
    last_pos = trail[-1]
    prev_pos = last_path[-1]
    optional: compute visibility center each step
    if (last_path[0], last_pos) intersects obstacle: # commit segment
        graph.add(path)
        paths.append([last_pos])
    else:
        last_path.append(last_pos)
    if (prev_pos, last_pos) intersects any previous path: # commit path
        determine whether follows or cross path
        if cross path:
            compute barycenter of the intersection or the visibility center
            balance paths at intersection using barycenter
            for ioldpath, ipos, iidx in intersected_paths:
                ipath.insert(iidx + 1, ipos)
            graph.add(path)
            paths.append([last_pos])
    """

# Planification

def compute_path():
    pass

# Simulation

def gen_view_real():
    mens_map = mens["environment"]["rawmap"]
    real_map = real["map"]

    inputs = [("view_real", [(pos, real_map[pos.intco]) for pos in get_sight()])]
    #pprint(inputs)
    return inputs

def update():
    # Reset perceptive memory
    mens["memory_perceptive"]["visual"] = []
    # Reset raycasts
    gui["agent_raycasts"] = []
    gui["pos_raycasts"] = []
    gui["vizcoef"] = {}

    # Shortcuts
    mens_map = mens["environment"]["rawmap"]
    real_map = real["map"]
    real_pos = real["pos"]
    real_direcq = real["direcq"]
    cpos = real_pos + .5

    # Compute robot sight range
    inputs = gen_view_real()

    process_inputs(mens, inputs)
    #print(mens_map)
    
    if real["motion"]["now"] == "move":
        find_local_visibility_center(cpos)
    
    real["motion"]["now"] = "none"
    
    

# GUI

TILE_SIZE = 80
GREY4 = (100, 100, 100)
BROWN5 = (200, 150, 50)
GREEN2 = (50, 250, 100)
GREEN8 = (0, 150, 50)
BLUE3 = (0, 150, 200)
BLUE7 = (50, 50, 200)
YELLOW1 = (255, 255, 100)
YELLOW7 = (150, 150, 0)
RED3 = (250, 50, 50)
RED7 = (200, 0, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

def gui_sdl_input(event):
    pos = real["pos"]
    direcq = real["direcq"]
    mapp = real["map"]
    xmax = len(real["map"]) - 1
    ymax = len(real["map"][0]) - 1
    if event.type == pygame.KEYDOWN:
        if event.key in [pygame.K_LEFT, pygame.K_RIGHT]:
            if event.key == pygame.K_LEFT:
                real["direcq"] = (direcq - 1) % 4
            elif event.key == pygame.K_RIGHT:
                real["direcq"] = (direcq + 1) % 4
            real["motion"]["now"] = "turn"
        if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
            if event.key == pygame.K_DOWN: direcq = (direcq + 2) % 4 # temp
            newpos = pos + direcq2vec(direcq)
            if no_collide_map(newpos):
                real["pos"] = newpos
            else:
                print("COLLISION", newpos)
            real["motion"]["now"] = "move"
        return True
    return False

def gui_sdl_draw(screen, font):
    rmap = real["map"]
    mmap = mens["environment"]["rawmap"]
    cur_view = [pos for pos, _ in mens["memory_perceptive"]["visual"][1]]
    ipos = real["pos"]
    cpos = ipos + .5
    direcq = real["direcq"]
    direcv = direcq2vec(direcq)
    COLOR_MAP = {'P': BLUE7, 0: WHITE, 1: RED7}
    #print("cur_view:", cur_view)
    #print("mmap:", mmap)
    
    mdirecv = gui["mdirecv"]
    bdirecv = gui["bdirecv"]
    edirecv = gui["edirecv"]
    semi_angle = gui["semi_angle"]
    sight_range = gui["radius"]
    agent_raycasts = gui["agent_raycasts"]
    pos_raycasts = gui.get("pos_raycasts", [])
    vizcoefs = gui.get("vizcoef", {})

    screen.fill(BLACK)
    # Draw map
    for pos, val in mmap:
        if pos in cur_view:
            pygame.draw.rect(screen, COLOR_MAP[val],
                vmul([*pos, 1, 1], TILE_SIZE))
        else:
            pygame.draw.rect(screen, vmul(COLOR_MAP[val], .5),
                vmul([*pos, 1, 1], TILE_SIZE))
    
    # Draw sight range 
    # pygame.draw.line(screen, BROWN5, cpos * TILE_SIZE, (cpos + bdirecv.magnified(sight_range)) * TILE_SIZE, 2)
    # pygame.draw.line(screen, BROWN5, cpos * TILE_SIZE, (cpos + edirecv.magnified(sight_range)) * TILE_SIZE, 2)
    # pygame.draw.circle(screen, BROWN5, (cpos * TILE_SIZE).trunc(), sight_range * TILE_SIZE, 2)
    #arc_rect = (*((cpos - sight_range) * TILE_SIZE), *(Vec2d(2, 2) * TILE_SIZE * sight_range))
    #pygame.draw.arc(screen, BROWN5, arc_rect, bdirecv.angle, edirecv.angle, 2)
    
    # Draw rays
    # pygame.draw.polygon(screen, GREEN8, [cpos * TILE_SIZE] + [(cpos + r) * TILE_SIZE for _, r, _ in agent_raycasts], 2)
    # pygame.draw.polygon(screen, YELLOW1, [cpos * TILE_SIZE] + [(cpos + r) * TILE_SIZE for _, r, _ in agent_raycasts], 0)
    # for coll_fpos, ray_vec, side in agent_raycasts:
        # pygame.draw.line(screen, GREEN8, cpos * TILE_SIZE, (cpos + ray_vec) * TILE_SIZE, 1)
        # line = side2edge(coll_fpos.trunc(), side)
        # pygame.draw.line(screen, GREEN8, line[0] * TILE_SIZE - 1, line[1] * TILE_SIZE - 1, 2)
    for ray_orig, color, (coll_fpos, ray_vec, side) in pos_raycasts[::-1]:
        pygame.draw.line(screen, color, ray_orig * TILE_SIZE, (ray_orig + ray_vec) * TILE_SIZE, 1)
        line = side2edge(coll_fpos.trunc(), side)
        pygame.draw.line(screen, color, line[0] * TILE_SIZE - 1, line[1] * TILE_SIZE - 1, 2)

    # Draw robot
    pygame.draw.rect(screen, COLOR_MAP['P'], vmul([*ipos, 1, 1], TILE_SIZE))
    pygame.draw.line(screen, WHITE, cpos * TILE_SIZE, (cpos + .5 * direcv) * TILE_SIZE, 4)
    pygame.draw.rect(screen, BLUE3, vmul([*ipos, .3, .3], TILE_SIZE))
    
    # Draw heading
    pygame.draw.line(screen, GREEN2, cpos * TILE_SIZE, (cpos + mdirecv) * TILE_SIZE, 4)
    
    # Draw map debug
    for pos in iproduct(range(len(rmap)), range(len(rmap[0]))):
        pos = Vec2d(pos)
        pygame.draw.rect(screen, GREEN2, [*((pos + .5) * TILE_SIZE - 1), 2, 2])
        vizcoef = vizcoefs.get(pos.intco)
        if vizcoef:
            font.render_to(screen, (pos * TILE_SIZE + 2).intco, "%.3G" % vizcoef, RED3)

    pygame.display.update()

def gui_sdl():
    pygame.display.init() #pygame.init() # no init audio which WTFs at 100% CPU
    pygame.freetype.init()
    screen = pygame.display.set_mode(vmul(real["map"].shape, TILE_SIZE))
    font = pygame.freetype.SysFont("DejaVu Sans Condensed", 16)
    clock = pygame.time.Clock()

    update()
    gui_sdl_draw(screen, font)
    dt = clock.tick(1)

    while True:
        processed_events = False
        for event in pygame.event.get():
            processed_events = gui_sdl_input(event) or processed_events
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        if processed_events:
            update()
            gui_sdl_draw(screen, font)

        # Tick and update caption
        dt = clock.tick(10)
        pygame.display.set_caption("Cognition Lab 2 | FPS {:.1f}".format(clock.get_fps()))
        #input()

# Entry point

def main():
    for k, v in {
    "memory_conceptual": {},
    "memory_episodic": {},
    "memory_procedural": {},
    "memory_perceptive": {"visual": {}},
    "memory_immediate": {},
    "context_spatiotemporal": {},
    "context_procedural": {},
    "context_conceptual": {},
    "environment": {"rawmap": set(), "map": set()},
    "sensors": yml("[eyes, skin, power_unit]"),
    "effectors": yml("[legs]"),
    }.items():
        mens[k] = v

    gui_sdl()

#g = nx.Graph({})
#nx.draw_networkx(g, pos={})
#plt.axis('off')
#plt.show()

if __name__ == "__main__":
    main()
