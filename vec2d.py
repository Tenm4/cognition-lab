#!/usr/bin/env python3
# -*- coding: Utf-8 -*-

"""
Defines 2D vectors and vector-related functions
"""

# Improved from pymunk's vec2d module
# @author = Gnoki

import operator
import math

class Vec2d(object):
    
    def __init__(self, x_or_pair=None, y = None):
        if x_or_pair != None:
            if y == None:
                it = iter(x_or_pair)
                self.x = next(it)
                self.y = next(it)
            else:
                self.x = x_or_pair
                self.y = y
 
    def __hash__(self):
        return hash((self.x, self.y))
 
    def __len__(self):
        return 2
 
    def __getitem__(self, key):
        if key == 0:
            return self.x
        elif key == 1:
            return self.y
        else:
            raise IndexError("Invalid subscript "+str(key)+" to Vec2d")
 
    def __setitem__(self, key, value):
        if key == 0:
            self.x = value
        elif key == 1:
            self.y = value
        else:
            raise IndexError("Invalid subscript "+str(key)+" to Vec2d")
 
    # String representation (for debugging)
    def __repr__(self):
        return 'Vec2d(%.1f, %.1f)' % (self.x, self.y)
    
    # Comparison
    def __eq__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x == other[0] and self.y == other[1]
        else:
            return False
    
    def __ne__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x != other[0] or self.y != other[1]
        else:
            return True
        
    def __gt__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x > other[0] and self.y > other[1]
        else:
            return False
        
    def __ge__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x >= other[0] and self.y >= other[1]
        else:
            return False
    
    def __lt__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x < other[0] and self.y < other[1]
        else:
            return False
        
    def __le__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x <= other[0] and self.y <= other[1]
        else:
            return False
 
    def __bool__(self): # TODO: fix float compare
        return self.x != 0.0 or self.y != 0.0
 
    # Generic operator handlers
    def _o2(self, other, f):
        "Any two-operator operation where the left operand is a Vec2d"
        if isinstance(other, Vec2d):
            return Vec2d(f(self.x, other.x),
                         f(self.y, other.y))
        elif (hasattr(other, "__getitem__")):
            return Vec2d(f(self.x, other[0]),
                         f(self.y, other[1]))
        else:
            return Vec2d(f(self.x, other),
                         f(self.y, other))
 
    def _r_o2(self, other, f):
        "Any two-operator operation where the right operand is a Vec2d"
        if (hasattr(other, "__getitem__")):
            return Vec2d(f(other[0], self.x),
                         f(other[1], self.y))
        else:
            return Vec2d(f(other, self.x),
                         f(other, self.y))
 
    def _io(self, other, f):
        "inplace operator"
        if (hasattr(other, "__getitem__")):
            self.x = f(self.x, other[0])
            self.y = f(self.y, other[1])
        else:
            self.x = f(self.x, other)
            self.y = f(self.y, other)
        return self
 
    # Addition
    def __add__(self, other):
        if isinstance(other, Vec2d):
            return Vec2d(self.x + other.x, self.y + other.y)
        elif hasattr(other, "__getitem__"):
            return Vec2d(self.x + other[0], self.y + other[1])
        else:
            return Vec2d(self.x + other, self.y + other)
    __radd__ = __add__
    
    def __iadd__(self, other):
        if isinstance(other, Vec2d):
            self.x += other.x
            self.y += other.y
        elif hasattr(other, "__getitem__"):
            self.x += other[0]
            self.y += other[1]
        else:
            self.x += other
            self.y += other
        return self
 
    # Subtraction
    def __sub__(self, other):
        if isinstance(other, Vec2d):
            return Vec2d(self.x - other.x, self.y - other.y)
        elif (hasattr(other, "__getitem__")):
            return Vec2d(self.x - other[0], self.y - other[1])
        else:
            return Vec2d(self.x - other, self.y - other)
    def __rsub__(self, other):
        if isinstance(other, Vec2d):
            return Vec2d(other.x - self.x, other.y - self.y)
        if (hasattr(other, "__getitem__")):
            return Vec2d(other[0] - self.x, other[1] - self.y)
        else:
            return Vec2d(other - self.x, other - self.y)
    def __isub__(self, other):
        if isinstance(other, Vec2d):
            self.x -= other.x
            self.y -= other.y
        elif (hasattr(other, "__getitem__")):
            self.x -= other[0]
            self.y -= other[1]
        else:
            self.x -= other
            self.y -= other
        return self
 
    # Multiplication
    def __mul__(self, other):
        if isinstance(other, Vec2d):
            return Vec2d(self.x*other.x, self.y*other.y)
        if (hasattr(other, "__getitem__")):
            return Vec2d(self.x*other[0], self.y*other[1])
        else:
            return Vec2d(self.x*other, self.y*other)
    __rmul__ = __mul__
    
    def __imul__(self, other):
        if isinstance(other, Vec2d):
            self.x *= other.x
            self.y *= other.y
        elif (hasattr(other, "__getitem__")):
            self.x *= other[0]
            self.y *= other[1]
        else:
            self.x *= other
            self.y *= other
        return self
 
    # Division
    def __div__(self, other):
        return self._o2(other, operator.div)
    def __rdiv__(self, other):
        return self._r_o2(other, operator.div)
    def __idiv__(self, other):
        return self._io(other, operator.div)
 
    def __floordiv__(self, other):
        return self._o2(other, operator.floordiv)
    def __rfloordiv__(self, other):
        return self._r_o2(other, operator.floordiv)
    def __ifloordiv__(self, other):
        return self._io(other, operator.floordiv)
 
    def __truediv__(self, other):
        return self._o2(other, operator.truediv)
    def __rtruediv__(self, other):
        return self._r_o2(other, operator.truediv)
    def __itruediv__(self, other):
        return self._io(other, operator.truediv)
 
    # Modulo
    def __mod__(self, other):
        return self._o2(other, operator.mod)
    def __rmod__(self, other):
        return self._r_o2(other, operator.mod)
 
    def __divmod__(self, other):
        return self._o2(other, divmod)
    def __rdivmod__(self, other):
        return self._r_o2(other, divmod)
 
    # Exponentation
    def __pow__(self, other):
        return self._o2(other, operator.pow)
    def __rpow__(self, other):
        return self._r_o2(other, operator.pow)
    
    # Bitwise
    def __and__(self, other):
        return self._o2(other, operator.and_)
    __rand__ = __and__

    def __or__(self, other):
        return self._o2(other, operator.or_)
    __ror__ = __or__
 
    def __xor__(self, other):
        return self._o2(other, operator.xor)
    __rxor__ = __xor__

    def __lshift__(self, n):
        return Vec2d((self.x << n), (self.y << n))
        
    def __rshift__(self, n):
        return Vec2d((self.x >> n), (self.y >> n))

    # Unary operations
    def __neg__(self):
        return Vec2d(operator.neg(self.x), operator.neg(self.y))
 
    def __pos__(self):
        return Vec2d(operator.pos(self.x), operator.pos(self.y))
 
    def __abs__(self):
        return Vec2d(abs(self.x), abs(self.y))
 
    def __invert__(self):
        return Vec2d(-self.x, -self.y)
    
    def __reversed__(self):
        return Vec2d(self.y, self.x)

    def __round__(self, n):
        return Vec2d(round(self.x, n), round(self.y, n))
        
    def trunc(self):
        return Vec2d(int(math.floor(self.x)), int(math.floor(self.y)))
        
    def floor(self):
        return Vec2d(math.floor(self.x), math.floor(self.y))
        
    def ceil(self):
        return Vec2d(math.ceil(self.x), math.ceil(self.y))
    
    def abstrunc(self):
        return Vec2d(int(self.x), int(self.y))
    
    def absfloor(self):
        return Vec2d(math.floor(self.x) + (self.x < 0), math.floor(self.y) + (self.y < 0))
    
    def absceil(self):
        return Vec2d(math.ceil(self.x) - (self.x < 0), math.ceil(self.y) - (self.y < 0))
    
    def signum(self):
        signum = lambda x: x and (1, -1)[x < 0] if x != math.nan else math.nan
        return self.map(signum)
    
    def map(self, f):
        return Vec2d(map(f, self))
    
    ### vector functions ###
    
    def get_magnitude_sqrd(self): 
        """Get the squared magnitude of the vector.
        It is more efficent to use this method for magnitude comparisons.
        
        :return: The squared magnitude
        """
        return self.x**2 + self.y**2
 
    def get_magnitude(self):
        """Get the magnitude of the vector.
        
        :return: The magnitude
        """
        return math.sqrt(self.x**2 + self.y**2) 
        
    def set_magnitude(self, value):
        """Set the magnitude of the vector. May raise a ZeroDivisionError"""
        magnitude = self.get_magnitude()
        self.x *= value/magnitude
        self.y *= value/magnitude
        
    magnitude = property(get_magnitude, set_magnitude, 
        doc = """Gets or sets the magnitude of the vector""")
    
    def magnified(self, value):
        """Return a new vector with the same direction and the magntude set to value."""
        ratio = value / self.get_magnitude()
        return Vec2d(self.x * ratio, self.y * ratio)
    
    def get_angle(self):
        if (self.get_magnitude_sqrd() == 0):
            return 0
        return math.atan2(self.y, self.x)
        
    def set_angle(self, angle):
        self.x = self.magnitude
        self.y = 0
        self.rotate(angle)
        
    angle = property(get_angle, set_angle, 
        doc="""Gets or sets the angle (in radians) of a vector""")
    
    def rotate(self, angle_radians):
        """Rotate the vector by angle_radians radians."""
        cos = math.cos(angle_radians)
        sin = math.sin(angle_radians)
        x = self.x*cos - self.y*sin
        y = self.x*sin + self.y*cos
        self.x = x
        self.y = y
    
    def rotated(self, angle_radians):
        """Return a new vector by rotating this vector by angle_radians radians.
        
        :return: Rotated vector
        """
        cos = math.cos(angle_radians)
        sin = math.sin(angle_radians)
        x = self.x*cos - self.y*sin
        y = self.x*sin + self.y*cos
        return Vec2d(x, y)
    
    def angle_to(self, other):
        """Get the angle between the vector and the other in radians
        
        :return: The angle
        """
        cross = self.x*other[1] - self.y*other[0]
        dot = self.x*other[0] + self.y*other[1]
        return math.atan2(cross, dot)
        
    def dot(self, other):
        """The dot product between two vectors
            v1.dot(v2) -> v1.x*v2.x + v1.y*v2.y
            
        :return: The dot product
        """
        return float(self.x*other[0] + self.y*other[1])
        
    def cross(self, other):
        """The 2D version of the cross product between two vectors,
        e.g returns a scalar corresponding to the magnitude of the result vector
            v1.cross(v2) -> v1.x*v2.y - v2.y*v1.x
        
        :return: The cross product
        """
        return self.x*other[1] - self.y*other[0]
    
    def normalize(self):
        """Normalize the vector and return its magnitude before the normalization
        
        :return: The magnitude before the normalization
        """
        magnitude = self.magnitude
        if magnitude != 0:
            self /= magnitude
        return magnitude
    
    def normalized(self):
        """Get a normalized copy of the vector
        Note: This function will return 0 if the magnitude of the vector is 0.
        
        :return: A normalized vector
        """
        magnitude = self.magnitude
        if magnitude != 0:
            return self/magnitude
        return Vec2d(self)

    def perprness(self, other):
        """How much right-perpendicular is the vector from other.
        Return a scalar that is > 0 if right, < 0 if left
        
        :return: boolean
        """
        return self.perpr().dot(other) # Computation is inverted on purpose

    def perpl(self):
        """Return a perpendicular vector (rotated PI/2 anticlockwise)
        
        :return: the perpendicular vector
        """
        return Vec2d(-self.y, self.x)
        
    def perpr(self):
        """Return a perpendicular vector (rotated PI/2 anticlockwise)
        
        :return: the perpendicular vector
        """
        return Vec2d(self.y, -self.x)
    
    def perpl_normalized(self):
        """Return a perpendicular and normalized vector
        
        :return: the perpendicular and normalized vector
        """
        magnitude = self.magnitude
        if magnitude != 0:
            return Vec2d(-self.y/magnitude, self.x/magnitude)
        return Vec2d(self)
    
    def perpr_normalized(self):
        """Return a perpendicular and normalized vector
        
        :return: the perpendicular and normalized vector
        """
        magnitude = self.magnitude
        if magnitude != 0:
            return Vec2d(self.y/magnitude, -self.x/magnitude)
        return Vec2d(self)
    
    def reflect(self, normal):
        """Reflect the vector of a given normal.
        Normal vector should be normalized."""
        dot_product = self.dot(normal)
        self.x -= 2 * normal.x * dot_product
        self.y -= 2 * normal.y * dot_product
        
    def reflected(self, normal):
        """Returns a vector reflected of a given normal.
        
        :return: the reflected vector
        """
        dot_product = self.dot(normal)
        x, y = self._get_int_coordinates()
        x -= 2 * normal.x * dot_product
        y -= 2 * normal.y * dot_product
        return Vec2d(x, y)
    
    def projected_on(self, other):
        """Return a vector that is the projection of the vector on the other
        
        :return: the projected vector
        """
        return other * (self.dot(other) / other.get_magnitude_sqrd())

    def scalar_projection(self, other):
        """Return the length of the projection of the vector on the other
        
        :return: the scalar projection
        """
        return self.dot(other) / other.get_magnitude_sqrd()
        
    def distance(self, other):
        """The distance between the vector and other vector
        
        :return: The distance
        """
        return math.sqrt((self.x - other[0])**2 + (self.y - other[1])**2)
        
    def distance_sqrd(self, other):
        """The squared distance between the vector and other vector
        It is more efficent to use this method for distance comparison.
        
        :return: The squared distance
        """
        return (self.x - other[0])**2 + (self.y - other[1])**2
    
    def interpolate(self, other, range=1):
        """Return a new vector by interpolating between 'self' and
        'other', 'range' times
        
        :return: the interpolated vector
        """
        self += (other - self) * range

    def interpolated(self, other, range=1):
        """Return a new vector by interpolating between 'self' and
        'other', 'range' times
        
        :return: the interpolated vector
        """
        return Vec2d(self + ((other - self) * range))
    
    def interpolated2(self, other, range):
        """Return a new vector by interpolating between 'self' and
        'other' by a translation vector whose magnitude is equal to 'range'
        
        :return: the interpolated vector
        """
        if self == other:
            return self
        diff = Vec2d(other - self)
        if range <= diff.magnitude:
            return Vec2d(self + (diff / diff.magnitude * range))
        else:
            return other
    
    def convert_to_basis(self, x_vector, y_vector):
        """Convert the vector into the basis defined by two vectors.
        Does not check colinearity of the two vectors"""
        x = self.dot(x_vector) / x_vector.get_magnitude_sqrd()
        y = self.dot(y_vector) / y_vector.get_magnitude_sqrd()
        return Vec2d(x, y)
    
    def _get_int_coordinates(self):
        return tuple(self.trunc())
    intco = property(_get_int_coordinates,  
        doc="""Return int coordinates of this vector""")
        
    def _get_float_coordinates(self):
        return self.x, self.y
    floatco = property(_get_float_coordinates,  
        doc="""Return float coordinates of this vector""")
    
    @staticmethod
    def zero():
        """A vector of zero magnitude"""
        return Vec2d(0, 0)
        
    @staticmethod
    def x_unit():
        """A unit vector pointing right"""
        return Vec2d(1, 0)
    
    @staticmethod
    def y_unit():
        """A unit vector pointing up"""
        return Vec2d(0, 1)
        
    @staticmethod
    def ones():
        """A vector where both x and y is 1"""
        return Vec2d(1, 1)
        

class Vec2(tuple):
    """Immutable vector of two ints"""
    
    def __new__(cls, x_or_pair, y = None):
        if y == None:
            return super(Vec2, cls).__new__(cls, x_or_pair)
        else:
            return super(Vec2, cls).__new__(cls, (x_or_pair, y))
    
    def __init__(self, x, y):
        self.x = x
        self.y = y
 
    def __len__(self):
        return 2
 
    def __getitem__(self, key):
        if key == 0:
            return self.x
        elif key == 1:
            return self.y
        else:
            raise IndexError("Invalid subscript "+str(key)+" to Vec2")
 
 
    # String representation (for debugging)
    def __repr__(self):
        return 'Vec2(%d,%d)' % (self.x, self.y)
    
    def __hash__(self):
        return tuple.__hash__(self)
    
    # Comparison
    def __eq__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x == other[0] and self.y == other[1]
        else:
            return False
    
    def __ne__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x != other[0] or self.y != other[1]
        else:
            return True
        
    def __gt__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x > other[0] and self.y > other[1]
        else:
            return False
        
    def __ge__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x >= other[0] and self.y >= other[1]
        else:
            return False
    
    def __lt__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x < other[0] and self.y < other[1]
        else:
            return False
        
    def __le__(self, other):
        if hasattr(other, "__getitem__") and len(other) == 2:
            return self.x <= other[0] and self.y <= other[1]
        else:
            return False
 
    def __bool__(self):
        return self.x != 0.0 or self.y != 0.0
 
    # Generic operator handlers
    def _o2(self, other, f):
        "Any two-operator operation where the left operand is a Vec2"
        if isinstance(other, Vec2):
            return Vec2(f(self.x, other.x),
                         f(self.y, other.y))
        elif (hasattr(other, "__getitem__")):
            return Vec2(f(self.x, other[0]),
                         f(self.y, other[1]))
        else:
            return Vec2(f(self.x, other),
                         f(self.y, other))
 
    def _r_o2(self, other, f):
        "Any two-operator operation where the right operand is a Vec2"
        if (hasattr(other, "__getitem__")):
            return Vec2(f(other[0], self.x),
                         f(other[1], self.y))
        else:
            return Vec2(f(other, self.x),
                         f(other, self.y))
 
    # Addition
    def __add__(self, other):
        if isinstance(other, Vec2):
            return Vec2(self.x + other.x, self.y + other.y)
        elif hasattr(other, "__getitem__"):
            return Vec2(self.x + other[0], self.y + other[1])
        else:
            return Vec2(self.x + other, self.y + other)
    __radd__ = __add__
 
    # Subtraction
    def __sub__(self, other):
        if isinstance(other, Vec2):
            return Vec2(self.x - other.x, self.y - other.y)
        elif (hasattr(other, "__getitem__")):
            return Vec2(self.x - other[0], self.y - other[1])
        else:
            return Vec2(self.x - other, self.y - other)
    def __rsub__(self, other):
        if isinstance(other, Vec2):
            return Vec2(other.x - self.x, other.y - self.y)
        if (hasattr(other, "__getitem__")):
            return Vec2(other[0] - self.x, other[1] - self.y)
        else:
            return Vec2(other - self.x, other - self.y)
 
    # Multiplication
    def __mul__(self, other):
        if isinstance(other, Vec2):
            return Vec2(self.x*other.x, self.y*other.y)
        if (hasattr(other, "__getitem__")):
            return Vec2(self.x*other[0], self.y*other[1])
        else:
            return Vec2(self.x*other, self.y*other)
    __rmul__ = __mul__
 
    # Division
    def __div__(self, other):
        return self._o2(other, operator.div)
    def __rdiv__(self, other):
        return self._r_o2(other, operator.div)
 
    def __floordiv__(self, other):
        return self._o2(other, operator.floordiv)
    def __rfloordiv__(self, other):
        return self._r_o2(other, operator.floordiv)
 
    def __truediv__(self, other):
        return self._o2(other, operator.truediv)
    def __rtruediv__(self, other):
        return self._r_o2(other, operator.truediv)
 
    # Modulo
    def __mod__(self, other):
        return self._o2(other, operator.mod)
    def __rmod__(self, other):
        return self._r_o2(other, operator.mod)
 
    def __divmod__(self, other):
        return self._o2(other, divmod)
    def __rdivmod__(self, other):
        return self._r_o2(other, divmod)
 
    # Exponentation
    def __pow__(self, other):
        return self._o2(other, operator.pow)
    def __rpow__(self, other):
        return self._r_o2(other, operator.pow)
    
    # Bitwise   
    def __and__(self, other):
        return self._o2(other, operator.and_)
    __rand__ = __and__

    def __or__(self, other):
        return self._o2(other, operator.or_)
    __ror__ = __or__
 
    def __xor__(self, other):
        return self._o2(other, operator.xor)
    __rxor__ = __xor__

    def __lshift__(self, n):
        return Vec2((self.x << n), (self.y << n))
        
    def __rshift__(self, n):
        return Vec2((self.x >> n), (self.y >> n))

    # Unary operations
    def __neg__(self):
        return Vec2(operator.neg(self.x), operator.neg(self.y))
 
    def __pos__(self):
        return Vec2(operator.pos(self.x), operator.pos(self.y))
 
    def __abs__(self):
        return Vec2(abs(self.x), abs(self.y))
 
    def __invert__(self):
        return Vec2(-self.x, -self.y)
    
    def __reversed__(self):
        return Vec2(self.y, self.x)

    def __round__(self, n):
        return Vec2(round(self.x, n), round(self.y, n))
        
    
    def floor(self):
        return Vec2(math.floor(self.x) if self.x > 0 else math.ceil(self.x), math.floor(self.y) if self.y > 0 else math.ceil(self.y))
    
    def ceil(self):
        return Vec2(math.ceil(self.x) if self.x > 0 else math.floor(self.x), math.ceil(self.y) if self.y > 0 else math.floor(self.y))

    ### vector functions ###
    
    def get_magnitude_sqrd(self): 
        """Get the squared magnitude of the vector.
        It is more efficent to use this method for magnitude comparisons.
        
        :return: The squared magnitude
        """
        return self.x**2 + self.y**2
 
    def _get_magnitude(self):
        """Get the magnitude of the vector.
        
        :return: The magnitude
        """
        return math.sqrt(self.x**2 + self.y**2) 
        
    def _set_magnitude(self, value):
        """Set the magnitude of the vector. May raise a ZeroDivisionError"""
        magnitude = self._get_magnitude()
        self.x *= value/magnitude
        self.y *= value/magnitude
        
    magnitude = property(_get_magnitude, _set_magnitude, 
        doc = """Gets or sets the magnitude of the vector""")
    
    def _get_angle(self):
        if (self.get_magnitude_sqrd() == 0):
            return 0
        return math.atan2(self.y, self.x)
        
    def _set_angle(self, angle):
        self.x = self.magnitude
        self.y = 0
        self.rotate(angle)
        
    angle = property(_get_angle, _set_angle, 
        doc="""Gets or sets the angle (in radians) of a vector""")
    
    def rotate(self, angle_radians):
        """Rotate the vector by angle_radians radians."""
        cos = math.cos(angle_radians)
        sin = math.sin(angle_radians)
        x = self.x*cos - self.y*sin
        y = self.x*sin + self.y*cos
        self.x = x
        self.y = y
    
    def rotated(self, angle_radians):
        """Return a new vector by rotating this vector by angle_radians radians.
        
        :return: Rotated vector
        """
        cos = math.cos(angle_radians)
        sin = math.sin(angle_radians)
        x = self.x*cos - self.y*sin
        y = self.x*sin + self.y*cos
        return Vec2(x, y)
    
    def angle_to(self, other):
        """Get the angle between the vector and the other in radians
        
        :return: The angle
        """
        cross = self.x*other[1] - self.y*other[0]
        dot = self.x*other[0] + self.y*other[1]
        return math.atan2(cross, dot)
        
    def dot(self, other):
        """The dot product between two vectors
            v1.dot(v2) -> v1.x*v2.x + v1.y*v2.y
            
        :return: The dot product
        """
        return float(self.x*other[0] + self.y*other[1])
    
    @staticmethod
    def zero():
        """A vector of zero magnitude"""
        return Vec2(0, 0)
        
    @staticmethod
    def x_unit():
        """A unit vector pointing right"""
        return Vec2(1, 0)
    
    @staticmethod
    def y_unit():
        """A unit vector pointing up"""
        return Vec2(0, 1)
        
    @staticmethod
    def ones():
        """A vector where both x and y is 1"""
        return Vec2(1, 1)
